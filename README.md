libmsgque
=========
# **消息队列(MESSAGE QUEUE)库项目简析** #

*注: 本文假设你已经有linux开发环境*

请确保你使用本库时是tag版本。<br>

> target=libmsgque-1.0

**本项目采用 GPL 授权协议，欢迎大家在这个基础上进行改进，并与大家分享，为开源事业贡献一点点力量。**

下面将简单的解析下项目：

## **一、项目的目录结构** ##
> 根目录<br>
> |-- src<br>
> |-- util<br>
> |-- build-aux<br>
>  `- m4<br>

**1、src目录**<br>
src目录用于存放项目的包及C源码文件。

下面是src目录的文件:
> src<br>
> |-- msgque.c<br>
> `-- msgque.h<br>

- msgque.c —库中的主文件，定义函数 
- msgque.h —库中的公共头文件，声明函数和结构体等

**2、util目录**<br>
util目录用于存放测试库的套件程序。

下面是util目录里的文件:
> util<br>
> |-- recv-test.c<br>
> '-- send-test.c<br>
> '-- recv-timeout.c<br>
> '-- send-timeout.c<br>

- recv-test.c —消息队列接收测试程序, 与下面的程序配合使用
- send-test.c —消息队列发送测试程序, 与上面的程序配合使用
- recv-timeout.c —消息队列接收超时测试程序, 与下面的程序配合使用
- send-timeout.c —消息队列发送测试程序, 与上面的程序配合使用

**3、build-aux目录**<br>
辅助文件。<br>
略。

**4、m4目录**<br>
辅助文件。<br>
略。

## **二、编译流程** ##
**1) 下载源代码**<br>
> $ git clone https://git.oschina.net/fulinux/libmsgque.git

**2) 进入libmsgque目录中**<br>
> $ cd libmsgque<br>

**3) 查看tag版本**<br>
> $ git tag

**4) 选择最新的tag**<br>
> $ git checkout vx.x.x

*注上面命令行中的"x"代表最新的tag版本中的版本号码*

**5) 配置选项, 检测环境、依赖关系等，然后编译**<br>
> $ ./configure && make<br>

*注: 当然你可以根据实际需要，为configure添加选项，例如加上--enable-static选项以使能静态库编译选项*

## **三、测试库套件使用** ##
**1) 进入test目录**<br>
> $ cd test<br>

**2) 在一个终端中运行接收函数<br>
> $ ./recv-test<br>

**3) 在另一个终端中运行发送函数<br>
> $ ./send-test<br>

## **四、libmsgque库主页以及邮件列表** ##

**库主页用于详细说明库的相关内容，大家亦可以在issues和博客下面反馈你使用过程中所遇到的问题和想法。希望大家能积极的改进该库，并在git@oschina上发起合并请求，如果确有改进，我会将其合并到master分支上来。当然你首先要是这个网站的用户，并且需要fork该库项目。**<br>
**你也可以直接通过邮件的方式将问题和想法发给我。**<br>

**1)库主页**<br>
**http://blog.csdn.net/fulinus/article/details/38947603**<br>

**2)邮件列表**<br>
**E-mail: fulinux@sina.com**<br>
**author: fulinux**<br>