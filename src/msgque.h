/********************************************************************************
 *      Copyright:  (C) 2014 EAST
 *                  All rights reserved.
 *
 *       Filename:  msgque.h
 *    Description:  这是消息队列的公共头文件，
 *                  应用声明一些库的API、宏和数据结构体等。 
 *
 *        Version:  1.0.0(08/14/2014)
 *         Author:  fulinux <fulinux@sina.com>
 *      ChangeLog:  1, Release initial version on "08/14/2014 11:31:33 AM"
 *                 
 ********************************************************************************/
#ifndef _MSGQUE_H_
#define _MSGQUE_H_

#define MSGSIZE 515

#ifndef TRUE
#define TRUE 1
#endif

#ifndef FALSE
#define FALSE 0
#endif

struct msgbuf {
    long mtype;
    char mtext[MSGSIZE];
};

typedef struct msgbuf msgbuf_t;

struct msgque {
    int fd;
    int mode;
    key_t key;                  
    int debug;
    int msgid;
    msgbuf_t *msgbuf;
};

typedef struct msgque msgque_t;

msgque_t *msgque_init (int fid, mode_t mode);
int msgque_setdebug(msgque_t *msgque, int flag);
int msgque_read (msgque_t *msgque, int msgflg);
int msgque_read_timeout (msgque_t *msgque, struct timeval *timeout);
int msgque_write (msgque_t *msgque, int msgflg);
int msgque_exit (msgque_t *msgque);

#endif /* _MSGQUE_H_ */
